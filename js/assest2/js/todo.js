$(document).ready(function(){
    var i = 1;
    $(document).on('click','#add-todo',function(){
        var inputValue = $('#input-default').val();
        var element = `<div class="m-top" id="container${i}">
        <input type="text" id="input${i}" name="input">
        <button id = "${i}" data-btn-id="${i}" class="remove-btn">X</button>
        </div>`;
        if ($('input').val()=="") {
            alert('You should enter your todo');
            return false;
        }else{
            $('#container').append(element);
            $('.remove-btn').css({' font-size': '20px',
            'font-weight': 'bold',
            'padding': '0px 10px',
            'background-color': 'purple',
            'color': 'white',
            'margin-top': '10px'});
            i++;
        }
    });
    $(document).on('click', '.remove-btn', function(){
        var remove = $(this).data('btn-id');
        $(`#container${remove}`).remove();
        
    });

})