$(document).on('click','#fromFahrenheit',function(){
    var f = Number($('#fahrenheit').val());
    if (f == "") {
        alert("please enter value to convert");
        return false;
    }
    c = Math.round((f - 32) * 5/9);
    $('#celsius').val(c);
    k = Math.round((f - 32) * 5/9 + 273.15);
    $('#kelvin').val(k);
    r = Math.round(f + 459.67);
    $('#rankine').val(r);
})
$(document).on('click','#fromRankine',function(){
    var r = Number($('#rankine').val());
    if (r == "") {
        alert("please enter value to convert");
        return false;
    }
    f = Math.round(r - 459.67);
    $('#fahrenheit').val(f);
    c = Math.round((r - 491.67) * 5/9);
    $('#celsius').val(c);
    k = Math.round((r) * 5/9);
    $('#kelvin').val(k);
})
$(document).on('click','#fromCelsius',function(){
    var c = Number($('#celsius').val());
    if (c == "") {
        alert("please enter value to convert");
        return false;
    }
    f = Math.round((c * (9/5)) + 32);
    $('#fahrenheit').val(f);
    k = Math.round(c + 273.15);
    $('#kelvin').val(k);
    r = Math.round((c + 273.15) * 9/5);
    $('#rankine').val(r);
})
$(document).on('click','#fromKelvin',function(){
    var k = Number($('#kelvin').val()); 
    if (k == "") {
        alert("please enter value to convert");
        return false;
    }
    f = Math.round((k - 273.15) * 9/5 + 32);
    $('#fahrenheit').val(f);
    c = Math.round(k - 273.15);
    $('#celsius').val(c);
    r = Math.round(k * 1.8);
    $('#rankine').val(r);
})
$(document).on('click','#reset',function(){
    $('#fahrenheit').val("");
    $('#rankine').val("");
    $('#kelvin').val("");
    $('#celsius').val("");
})
