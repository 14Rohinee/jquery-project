function formValidation() {
    var companyNameValue = document.getElementById("companyName").value;
    var companyEmailValue = document.getElementById("companyMail").value;
    var passwordValue = document.getElementById("password").value;
    var confirmPasswordValue = document.getElementById("confirmPassword").value;
    var passwordValidation = (/^[0-9A-Za-z]+$/).test(passwordValue);
    
    if (companyNameValue == "" || companyEmailValue == "" || passwordValue == "" || confirmPasswordValue == "") {
        alert("Please mention all the fields");
    }else if (companyNameValue.length <= 15) {
        alert("company name length should be greater than 15");
        return false;
    }else if (!passwordValidation) {
        alert(" Please enter alpha numeric password");
        return false;
    }else if(passwordValue.length != 6){
        alert("Please enter 6 digit alpha numeric password");
        return false;
    }
    else if(confirmPasswordValue != passwordValue){
        alert("Enter same password"); 
        return false;
    }
}